package crawler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"

	"bitbucket.org/qingsam/web-crawler/model"
	"golang.org/x/net/html"
)

// Crawler contains the logic for crawling a given url
type Crawler struct {
}

// Start the crawler and print the results out
func (c *Crawler) Start(url string, domainToCrawl string, depth int) {
	crawlerResults := model.CrawlerResults{
		Results: make(map[string][]string),
		Visited: make(map[string]bool),
	}

	c.crawl(url, domainToCrawl, depth, &crawlerResults)

	// print pretty map
	b, err := json.MarshalIndent(crawlerResults.Results, "", "  ")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Print(string(b))
}

// Crawl extract all http links from a given url using the depth first search approuch
// TODO domainToCrawl can be a func that filters, so we can have multiple domains
func (c *Crawler) crawl(url string, domainToCrawl string, depth int, crawlerResults *model.CrawlerResults) (*model.CrawlerResults, error) {
	body, err := c.getBodyFromURL(url) // get io to access body content
	if err != nil {
		return nil, err
	}
	urls, err := c.crawlHelper(body) // get all the url links from the given url
	if err != nil {
		return nil, err
	}

	crawlerResults.Visited[url] = true // mark current url as visited
	crawlerResults.Results[url] = urls // assign results to the map

	if depth <= 0 {
		return crawlerResults, nil // return when final depth is reached
	}

	var urlsToCrawl = make([]string, 0, 5) // populate url to crawl
	for _, hrefURL := range urls {
		if strings.Contains(hrefURL, domainToCrawl) {
			urlsToCrawl = append(urlsToCrawl, hrefURL)
		}
	}

	// recursivly call the urls to get the next depth of results
	for _, urlToCrawl := range urlsToCrawl {
		if !crawlerResults.IsVisited(urlToCrawl) {
			c.crawl(urlToCrawl, domainToCrawl, depth-1, crawlerResults)
		}
	}

	return crawlerResults, nil
}

// helper to extract list of urls from the body
func (c *Crawler) crawlHelper(body io.ReadCloser) ([]string, error) {
	var urls []string

	z := html.NewTokenizer(body)
	defer body.Close()
	for {
		tt := z.Next()

		switch {
		case tt == html.ErrorToken:
			// end of the document, return all the url extracted
			return urls, nil
		case tt == html.StartTagToken:
			t := z.Token()

			// check for <a>
			isAnchor := t.Data == "a"
			if !isAnchor {
				continue
			}

			// extract the href value, if there is one
			ok, hrefURL := c.getHref(t)
			if !ok {
				continue
			}

			// check for a valid url
			hasProto := strings.Index(hrefURL, "http") == 0
			if hasProto {
				urls = append(urls, hrefURL)
			}
		}
	}
}

// get Href from the token attr
func (c *Crawler) getHref(t html.Token) (ok bool, href string) {
	for _, a := range t.Attr {
		if a.Key == "href" {
			href = a.Val
			ok = true
		}
	}
	return
}

// method get the ReadCloser to access the body content
func (c *Crawler) getBodyFromURL(url string) (io.ReadCloser, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	return resp.Body, err
}
