package crawler

import (
	"io/ioutil"
	"os"
	"testing"

	"bitbucket.org/qingsam/web-crawler/model"
)

func TestCrawlWithURL(t *testing.T) {
	c := Crawler{}
	crawlerResults := model.CrawlerResults{
		Results: make(map[string][]string),
		Visited: make(map[string]bool),
	}
	c.crawl("https://www.cuvva.com/", "cuvva.com", 0, &crawlerResults)

	actual := crawlerResults.Results["https://www.cuvva.com/"]
	expected := 34
	if len(actual) != expected {
		t.Errorf("Expected %v got %v", expected, len(actual))
	}
}

func TestCrawlWithDepthHigherThanZero(t *testing.T) {
	c := Crawler{}
	crawlerResults := model.CrawlerResults{
		Results: make(map[string][]string),
		Visited: make(map[string]bool),
	}
	c.crawl("https://www.cuvva.com/", "cuvva.com", 1, &crawlerResults)

	urls, _ := crawlerResults.Results["https://www.cuvva.com/"]
	actual := len(urls)
	if actual > 34 {
		t.Errorf("Expected number of URLs higher than 34 but got %v", actual)
	}
}

func TestCrawlWithNoURLInBody(t *testing.T) {
	htmlFile := "../test_files/example_no_URL.html"
	c := Crawler{}

	r, err := os.Open(htmlFile)
	if err != nil {
		t.Errorf("no html file: %v", htmlFile)
	}
	urls, _ := c.crawlHelper(ioutil.NopCloser(r))

	actual := len(urls)
	expected := 0
	if actual != expected {
		t.Errorf("Expected %v got %v", expected, actual)
	}
}

func TestCrawlWithURLsInBody(t *testing.T) {
	htmlFile := "../test_files/example_body.html"
	c := Crawler{}

	r, err := os.Open(htmlFile)
	if err != nil {
		t.Errorf("no html file: %v", htmlFile)
	}
	urls, _ := c.crawlHelper(ioutil.NopCloser(r))

	actual := len(urls)
	expected := 2
	if actual != expected {
		t.Errorf("Expected %v got %v", expected, actual)
	}
}
