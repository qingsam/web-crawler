# WEB-CRAWLER #
Simple web crawler that crawls a site given a url and depth parameter

#### Running the application

~~~~
{url} - the url to start the crawler
{domainToCrawl} - the domain to look out for when crawling
{depth} - how deep the crawler will go

go run main.go {url} {domainToCrawl} {depth}
go run main.go https://www.xyz.com/ xyz.com 1
~~~~


#### TODO
* Be good to have the crawler running in parallel so that we don't need to wait for each url to be crawled in sequence
* I believe this can be achieved through goRoutines and channels.
