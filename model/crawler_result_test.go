package model

import "testing"

func TestIsVisited(t *testing.T) {
	visitedURL := "www.google.com"
	results := make(map[string][]string)
	visited := map[string]bool{visitedURL: true}
	crawlerResults := CrawlerResults{results, visited}

	actual := crawlerResults.IsVisited(visitedURL)
	expected := true
	if actual != expected {
		t.Errorf("Expected %v got %v", expected, actual)
	}
}
