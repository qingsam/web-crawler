package model

// CrawlerResults hold the crawl results
type CrawlerResults struct {
	Results map[string][]string // stores the Map of (page_url -> urls_on_page)
	Visited map[string]bool     // stores the visited pages
}

// IsVisited checks if a given url is already been visited
func (c *CrawlerResults) IsVisited(url string) bool {
	return c.Visited[url]
}
