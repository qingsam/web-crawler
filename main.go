package main

import (
	"flag"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/qingsam/web-crawler/crawler"
)

func main() {

	flag.Parse()
	args := flag.Args()

	if len(args) < 1 {
		fmt.Println("Please specify url to crawl")
		os.Exit(1)
	}
	crawler := crawler.Crawler{}

	url := args[0]
	domainToCrawl := args[1]

	depth, _ := strconv.Atoi(args[2])

	crawler.Start(url, domainToCrawl, depth)
}
